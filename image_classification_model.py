from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
##from PIL import Image
from imutils import paths
import numpy as np
import os
import cv2

def extract_color_stats(image):
        print('extract_color_stats:----->'
        (R, G, B) = cv2.split(image)
        features = [np.mean(R), np.mean(G), np.mean(B), np.std(R),np.std(G), np.std(B)]
        print(features)
        print('extract_color_stats')
        return features
print('r----')

models = {
	"knn": KNeighborsClassifier(n_neighbors=1),
	"naive_bayes": GaussianNB(),
	"logit": LogisticRegression(solver="lbfgs", multi_class="auto"),
	"svm": SVC(kernel="linear"),
	"decision_tree": DecisionTreeClassifier(),
	"random_forest": RandomForestClassifier(n_estimators=100),
	"mlp": MLPClassifier()
        }
print('a----')
imagePaths = paths.list_images('input')
print(imagePaths)
data = []
labels = []

for imagePath in imagePaths:
        print('b----')
        print(imagePath)
        image=cv2.imread(imagePath)
        features = extract_color_stats(image)
        data.append(features)


        label = imagePath.split(os.path.sep)[-2]
        print(label)
        labels.append(label)

le = LabelEncoder()
labels = le.fit_transform(labels)
print('d-----')
(trainX, testX, trainY, testY) = train_test_split(data, labels,
	test_size=0.25)


model = models['knn']
print(model)
model.fit(trainX, trainY)

image=cv2.imread('football_image/3.jpg')
print('c----')
(R, G, B) = cv2.split(image)
features = [np.mean(R), np.mean(G), np.mean(B), np.std(R),np.std(G), np.std(B)]
testX=[features]
predictions = model.predict(testX)
print(predictions)




